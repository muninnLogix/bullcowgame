// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "BullCowCartridge.h"
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"

// Start Game on Terminal in Bulls and Cows
void UBullCowCartridge::BeginPlay() 
{
    Super::BeginPlay();

    // Load the a wordlist in WordLists directory on beginning play
    const FString wordListPath = FPaths::ProjectContentDir() / TEXT("WordLists/1000_common_words.txt");
    FFileHelper::LoadFileToStringArrayWithPredicate(FilteredWordList, *wordListPath, [](const FString& Word)
        {
            return Word.Len() <= 6 && Word.Len() > 2 && IsIsogram(Word);
        });
    
    // Initialize player lives and the hidden word
    SetupGame();
}

// After Player hits enter on terminal
void UBullCowCartridge::OnInput(const FString& PlayerGuess)
{
    ClearScreen();
    
    if ( bGameOver )
    {
        GameOver(PlayerGuess);
        return;
    }
    
    ProcessPlayerGuess(PlayerGuess, HiddenWord);
}


void UBullCowCartridge::SetupGame()
{
    // ClearScreen();
    
    HiddenWord = FString::Printf(TEXT("%s"), *RandomHiddenWord(FilteredWordList));
    
    if ( !IsIsogram(HiddenWord) )
    {
        UE_LOG(LogExec, Warning, TEXT("Word %s is not an isogram!!"), *HiddenWord);
        SetupGame(); // Re-roll the hidden word if it is not an isogram
        return;
    }
    
    PlayerLives = InitLives(HiddenWord);
    bGameOver = false;

    PrintBanner(HiddenWord, PlayerLives);
    // TODO: Create Cows for each life
}

FString UBullCowCartridge::RandomHiddenWord(const TArray<FString>& filteredWordList) const
{
    const int32 listLength = filteredWordList.Num();
    const int32 randIndex = FMath::RandRange(0, listLength - 1);
    const FString chosenWord = filteredWordList[randIndex];
    return chosenWord;
}


void UBullCowCartridge::ProcessPlayerGuess(const FString& Guess, const FString& Target)
{
    if ( Guess == Target )
    {
        ClearScreen();
        // How do I add a component to this component that will cause sparkles if we win?
        // Sparkles!!!
        PrintLine(TEXT("You guessed it!"));
        PrintLine(TEXT("How'd you do that?\n\n"));
        PrintLine(TEXT("Press Enter to continue."));
        bGameOver = true;
        return;
    }
    
    if ( PlayerLives < 1 )
    {
        GameOver(Guess);
        return;
    }

    if ( !DoesWordLengthMatch(Guess, Target) )
    {
        ClearScreen();
        PrintPlayerHints(Target, PlayerLives);
        PrintLine(TEXT("You entered: %s"), *Guess);
        PrintLine(TEXT("\nThat's not the same length as the word!"));
        return;
    }

    if ( !IsIsogram(Guess) )
    {
        ClearScreen();
        PrintPlayerHints(Target, PlayerLives);
        PrintLine(TEXT("You entered: %s"), *Guess);
        PrintLine(TEXT("\n%s is not an isogram! (no repeating letters)\nOoooh I feel the cows getting nervous!"), *Guess);
        return;
    }

    ClearScreen();
    DecreasePlayerChances(1);
    PrintPlayerHints(Target, PlayerLives);
    PrintLine(TEXT("You entered: %s"), *Guess);

    // Set chars that are bulls/cows
    TArray<char> bullsArray = GetBullsArray(Guess, Target);
    TArray<char> cowsArray = GetCowsArray(bullsArray, Guess, Target);

    FBullCowCount Score;
    // Print out Arrays if not empty
    if ( bullsArray.Num() > 0 )
    {
        Score.Bulls = FString::Printf(TEXT("Bulls: %s"), *ToStringArrayContents(bullsArray));
    }
    
    if ( cowsArray.Num() > 0 )
    {
        Score.Cows = FString::Printf(TEXT("Cows: %s"), *ToStringArrayContents(cowsArray));
    }

    PrintLine(TEXT("%s"), *Score.Bulls);
    PrintLine(TEXT("%s"), *Score.Cows);

    // If nothing else, then we also know that Guess != Target
    // How do I "explode" an actor?
    PrintLine(TEXT("\nINCORRECT!!!"));
    PrintLine(TEXT("Say goodbye to a chance..."));
    // Explode a cow
    // Add Screenshake
    PrintLine(TEXT("\nOH THE MOOMANITY!!!!"));
    PrintLine(TEXT("\nIsn't this fun?"));
    PrintLine(TEXT("Try Guessing again!"));
    // How do I remove an actor in response to something?
}

void UBullCowCartridge::PrintBanner(const FString& Word, const int32& lives) const
{
    PrintLine(TEXT("Guess the words!"));
    PrintLine(TEXT("It's easy!"));
    PrintLine(TEXT("But don't blame me if there are...consequences."));
    PrintLine(TEXT("Type your word and press enter to start!"));
    PrintLine(TEXT("\nHints: "));
    PrintLine(TEXT("Word has no repeated letters."));
    PrintLine(TEXT("Word is %i characters long."), Word.Len());
    PrintLine(TEXT("You have %i chances to guess."), lives);
    PrintLine(TEXT("\nIf letter of your guess is:"));
    PrintLine(TEXT("   Right spot AND right letter: It's a Bull!"));
    PrintLine(TEXT("   Right letter BUT wrong spot: It's a Cow!"));
}


int32 UBullCowCartridge::InitLives(const FString& word)
{
    int32 wordLength = word.Len();
    
    if ( wordLength < 5 )
    {
        return 5;
    }

    return wordLength;
}


void UBullCowCartridge::PrintPlayerHints(FString word, int32 lives) const
{
    // Give player hints
    int32 wordLength = word.Len();
    const bool debug = false;

    DebugMessaging(debug, word);
    PrintLine(TEXT("+ HINT: It's an isogram."));
    UE_LOG(LogExec, Warning, TEXT("Isogram Hint was called."));
    PrintLine(TEXT("+ HINT: Word length = %i"), wordLength);
    UE_LOG(LogExec, Warning, TEXT("Word length Hint was called."));

    if ( lives <= 2 )
    {
        PrintLine(TEXT("+ WARNING!! %i lives left!\n"), lives);
        return;
    }

    PrintLine(TEXT("+ HINT: You have %i lives.\n"), lives);
}


void UBullCowCartridge::DebugMessaging(const bool& debug, const FString& word) const
{
    UE_LOG(LogExec, Warning, TEXT("DebugMessaging was called."));
    if ( debug )
    {
        PrintLine(TEXT("DEBUG => word is: %s"), *word);
    }
}


bool UBullCowCartridge::DoesWordLengthMatch(const FString& playerWord, const FString& compare) const
{
    if ( playerWord.Len() == compare.Len() )
    {
        return true;
    }

    return false;
}


bool UBullCowCartridge::IsIsogram(const FString& word)
{
    // For each character of word
    int32 wordLength = word.Len(); // 1 less for the term bit

    for ( int32 selectedCharIndex = 0; selectedCharIndex < wordLength; ++selectedCharIndex )
    {
        for ( int32 charInString = selectedCharIndex + 1; charInString < wordLength; ++charInString )
        {
            // Check if that character matches any other character within the word EXCEPT itself
            if ( word[selectedCharIndex] == word[charInString] )
            {
                return false;
            }
        }
    }
    // If no matches, return true
    return true;
}


void UBullCowCartridge::DecreasePlayerChances(const int32& amount)
{
    PlayerLives -= amount;
}

// Sets gameover state and prompts player to Play again.
void UBullCowCartridge::GameOver(const FString& input)
{
    bGameOver = true;
    ClearScreen();
    PrintLine(TEXT("Game Over!!"));
    // Prompt player to play again
      // If yes re-run BeginPlay
    PrintLine(TEXT("Play again? (yes/no)"));
      // How do I exit this loop and start it again?
      // If No, exit game
      // How do I exit the game entirely?
    PrintLine(TEXT("\nWord was: %s"), *HiddenWord);
    if ( input.ToLower() == "yes")
    {
        ClearScreen();
        SetupGame();
    }
}


TArray<char> UBullCowCartridge::GetBullsArray(const FString& Guess, const FString& Target) const
{
    TArray<char> Bulls = {};

    for ( int32 i = 0; i < Guess.Len() && i < Target.Len(); ++i)
    {
        if ( Guess[i] == Target[i] )
        {
            Bulls.Add(Guess[i]);
        }
    }
    return Bulls;
}

TArray<char> UBullCowCartridge::GetCowsArray(const TArray<char>& currentBulls, const FString& guess, const FString& target) const
{
    TArray<char> Cows = {};
    // for each char in guess
    for ( char currentChar: guess )
    {
        // try to break early for term bit
        if ( !currentChar )
        {
            break;
        }
        // If current char of guess is in currentBulls, skip
        if ( currentBulls.Contains(currentChar) )
        {
            continue;
        }

        if ( target.Contains(FString::Printf(TEXT("%c"), currentChar)))
        {
            // else, if current Char IN target, emplace to Cows
            Cows.Add(currentChar);
        }
    }
    // return cows array
    return Cows;
}


FString UBullCowCartridge::ToStringArrayContents(const TArray<char>& targetArray) const
{
    FString response;
    if ( targetArray.Num() > 0 )
       {
           for ( char targetChar: targetArray )
           {
               response += FString::Printf(TEXT("%c "), targetChar);
           }
       }
    return response;
}
