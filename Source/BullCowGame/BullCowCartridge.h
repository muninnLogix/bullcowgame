// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Console/Cartridge.h"
#include "BullCowCartridge.generated.h"

struct FBullCowCount
{
	FString Bulls = "Bulls: None";
	FString Cows = "Cows: None";
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BULLCOWGAME_API UBullCowCartridge : public UCartridge
{
	GENERATED_BODY()

	public:
	virtual void BeginPlay() override;
	virtual void OnInput(const FString& PlayerGuess) override;

	// Your declarations go below!
	private:
	FString HiddenWord;
	int32 PlayerLives;
	bool bGameOver;
	// List of words that are able to be used for the game, unfiltered
	TArray<FString> WordList;
	// List of filtered words, i.e. <= 6 chars long, are isograms, etc.
	TArray<FString> FilteredWordList;
	
	// Init Game

	/* Helper functions that the Cartridge game uses, but that aren't a part of the game loop.
	 ********************************************/
	void DebugMessaging(const bool& debug, const FString& word) const;
	// Reduces Players lives by <amount>
	void DecreasePlayerChances(const int32& amount);
	// Returns true if word lengths of 2 words match
	bool DoesWordLengthMatch(const FString& playerWord,const FString& compare) const;
	// Starts Gameover screen and flips bGameOver => true
	void GameOver(const FString& input);
	// Generates an array of "bulls" given a guess and target
	TArray<char> GetBullsArray(const FString& Guess, const FString& Target) const;
	// Generates a char array of "Cows" given a guess and a target
	TArray<char> GetCowsArray(const TArray<char>& currentBulls, const FString& guess, const FString& target) const;
	// Returns True if no letters repeat within the given word
	static bool IsIsogram(const FString& word);
	// Sets player lives based on word length, minimum of 5
	int  InitLives(const FString& word);
	// Print a helpful welcome banner
	void PrintBanner(const FString& Word, const int32& lives) const;
	// Print Player Hints about word length and lives left
	void PrintPlayerHints(FString word, int32 lives) const;
	// Consumes and Array of chars and returns a space separated string containing chars from the array
	FString ToStringArrayContents(const TArray<char>& targetArray) const;
	// Tests Player guess against various conditions to determine if the Player has guessed our word.
	void ProcessPlayerGuess(const FString& Guess, const FString& Target);
	FString RandomHiddenWord(const TArray<FString>& filteredWordList) const;
	void SetupGame();
};
